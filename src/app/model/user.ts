export class User {
  id: number;
  firstname: string;
  surname: string;
  email: string;
  city: string;
  country: string;
  address: string;
  postcode: string;
  telephone: string;
}
