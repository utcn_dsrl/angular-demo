import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  usersURL = 'http://localhost:8080/spring-demo/user';

  constructor(private http: HttpClient) {
  }

  getUsers() {
    return this.http.get<User[]>(this.usersURL + '/all');
  }

  getUserById(id: number) {
    return this.http.get<User>(this.usersURL + '/details/' + id);
  }

}
